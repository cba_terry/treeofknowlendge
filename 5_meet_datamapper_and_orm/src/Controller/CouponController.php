<?php

use TreeOfKnowlendge\NamespaceAndPsr\Persistence as ORM;
use TreeOfKnowlendge\NamespaceAndPsr\Model as ORMODEL;

class CouponController
{
	public function createAction ()
	{
		$conn = new mysqli('host', 'user', 'password');

		if ($conn->connect_error)
			die(sprintf('Unable to connect to the database. %s', $conn->connect_error));

		// Tell Simple ORM to use the connection you just created.
		ORM\SimpleOrm::useConnection($conn, 'database');

		$coupon = new ORMODEL\Coupon();

		$coupon->id = uniqid();
		$coupon->name = 'Banh Trung Thu nhan dau xanh';
		$coupon->price = 5000;
		$coupon->type = ORMODEL\Coupon::FOR_PAY;
		$coupon->quanity = 100;

		$coupon->save();

	}
}