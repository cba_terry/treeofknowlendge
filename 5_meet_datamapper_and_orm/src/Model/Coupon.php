<?php

namespace TreeOfKnowlendge\NamespaceAndPsr\Model;

use TreeOfKnowlendge\NamespaceAndPsr\Persistence as ORM;

class Coupon extends ORM\SimpleOrm {
	
	public $id;
	public $name;
	public $price;
	public $type;
	public $quanity;

	const FOR_FREE = 0;
	const FOR_PAY = 1;

	public function isAvailable ()
	{
		$numberSaled = self::sql("SELECT COUNT(*) FROM coupon_order WHERE coupon = '" . $this->id "'");

		if($numberSaled >= $this->quanity)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}