<?php

namespace TreeOfKnowlendge\NamespaceAndPsr\Model;

class Coupon {
	
	public $name;
	public $price;
	public $type;

	const FOR_FREE = 0;
	const FOR_PAY = 1;

	public function __construct($name = null, $price = 0, $type = self::FOR_FREE)
	{
		if(!$name) 
			throw new \InvalidArgumentException("Empty coupon name");

		if($type === self::FOR_FREE && $price !== 0) 
			throw new \InvalidArgumentException("Coupon Free Must have price is zero");

		if($type === self::FOR_PAY && $price === 0)
			throw new \InvalidArgumentException("Coupon Pay Must have price greater than zero");
		
		$this->name = $name;
		$this->price = $price;
		$this->type = $type;
	}

}