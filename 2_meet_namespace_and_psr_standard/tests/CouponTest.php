<?php

use TreeOfKnowlendge\NamespaceAndPsr\Model\Coupon;

class CouponTest extends PHPUnit_Framework_Testcase
{
	/**
     * @expectedException        InvalidArgumentException
     * @expectedExceptionMessage Empty coupon name
     */

	public function test_coupon_must_have_a_name ()
	{
		$coupon = new Coupon();
	}

	/**
     * @expectedException        InvalidArgumentException
     * @expectedExceptionMessage Coupon Free Must have price is zero
     */

	public function test_coupon_free_must_have_price_zero ()
	{
		$coupon = new Coupon(
			'Banh Trung thu nhan dau xanh',
			5000,
			Coupon::FOR_FREE
			);
	}

	/**
     * @expectedException        InvalidArgumentException
     * @expectedExceptionMessage Coupon Pay Must have price greater than zero
     */

	public function test_coupon_pay_must_have_price_greater_than_zero ()
	{
		$coupon = new Coupon(
			'Banh Trung thu nhan dau xanh',
			0,
			Coupon::FOR_PAY
			);
	}

	public function test_a_valid_free_coupon ()
	{
		$coupon = new Coupon(
			'Banh Trung thu nhan dau xanh',
			0,
			Coupon::FOR_FREE
			);

		$this->assertEquals(0, $coupon->price);
	}

	public function test_a_valid_pay_coupon ()
	{
		$coupon = new Coupon(
			'Banh Trung thu nhan dau xanh',
			5000,
			Coupon::FOR_PAY
			);

		$this->assertEquals(5000, $coupon->price);
	}	
}