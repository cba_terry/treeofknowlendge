<?php

namespace TreeOfKnowlendge\NamespaceAndPsr\Model;

use TreeOfKnowlendge\NamespaceAndPsr\Repository\OrderRepository;

class Coupon {
	
	public $id;
	public $name;
	public $price;
	public $type;
	public $quanity;

	const FOR_FREE = 0;
	const FOR_PAY = 1;

	public function __construct($name = null, $price = 0, $type = self::FOR_FREE, $quanity = 999999)
	{
		if(!$name) 
			throw new \InvalidArgumentException("Empty coupon name");

		if($type === self::FOR_FREE && $price !== 0) 
			throw new \InvalidArgumentException("Coupon Free Must have price is zero");

		if($type === self::FOR_PAY && $price === 0)
			throw new \InvalidArgumentException("Coupon Pay Must have price greater than zero");
		

		$this->id = uniqid();
		$this->name = $name;
		$this->price = $price;
		$this->type = $type;
		$this->quanity = $quanity;
	}


	public function isAvailable (OrderRepository $orderRepository)
	{
		$numberSaled = $orderRepository->getNumberSaledByCoupon($this->id);

		if($numberSaled >= $this->quanity)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}