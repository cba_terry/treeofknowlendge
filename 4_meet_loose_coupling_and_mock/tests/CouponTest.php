<?php

use TreeOfKnowlendge\NamespaceAndPsr\Model\Coupon;
use TreeOfKnowlendge\NamespaceAndPsr\Repository\OrderRepository;

class CouponTest extends PHPUnit_Framework_Testcase
{
	private $coupon;

	public function setUp ()
	{
		$this->coupon = new Coupon(
				'Banh Trung thu nhan dau xanh',
				0,
				Coupon::FOR_FREE,
				100
			);
	}

	public function create_order_repository_stub ($expect_number_order)
	{
		// Create a stub for OrderRepository.
		
		// this not work on phpunit 4.8
		// $orderRepositoryStub =  $this->getMockBuilder('\\REPO\\OrderRepository')
		// 							->setMethods(array('getNumberSaledByCoupon'))
  		//                    		->getMock();

		$orderRepositoryStub = $this->getMock(
									'TreeOfKnowlendge\NamespaceAndPsr\Repository\OrderRepository',
									array('getNumberSaledByCoupon'),
									array(), 
									'', 
									false
								);

        // Configure the stub.
        $orderRepositoryStub->expects($this->any())
				        	->method('getNumberSaledByCoupon')
				            ->will($this->returnValue($expect_number_order));

		return $orderRepositoryStub;
	}

	public function test_coupon_is_available ()
	{	
		$orderRepositoryStub = $this->create_order_repository_stub(10);

		$this->assertTrue($this->coupon->isAvailable($orderRepositoryStub));
	}

	public function test_coupon_is_not_available ()
	{
		$orderRepositoryStub = $this->create_order_repository_stub(1000);

		$this->assertFalse($this->coupon->isAvailable($orderRepositoryStub));
	}
}