<?php

use TreeOfKnowlendge\NamespaceAndPsr\Model\Coupon;

class CouponTest extends PHPUnit_Framework_Testcase
{
	private $coupon;

	public function setUp ()
	{
		$this->coupon = new Coupon(
				'Banh Trung thu nhan dau xanh',
				0,
				Coupon::FOR_FREE,
				100
			);
	}

	public function test_coupon_is_available ()
	{
		$this->assertTrue($this->coupon->isAvailable());
	}
}