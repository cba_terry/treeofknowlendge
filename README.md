#Install PHPUnit
~~~~
> wget https://phar.phpunit.de/phpunit.phar
> chmod +x phpunit.phar
> sudo mv phpunit.phar /usr/local/bin/phpunit
> phpunit --version
~~~~

#To run a example
~~~~
> cd /path/to/TreeOfKnowlendge/[ex:1_meet_phpunit_and_unittest]
> phpunit tests/.
~~~~
